package fr.gouv.vitamui.pastis.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class PastisConfiguration {

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer () {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("http://10.100.129.51","http://vps795748.ovh.net",
                                "http://localhost:4200", "http://145.239.92.134")
                        .allowedMethods("POST", "GET","PUT");
            }
        };
    }
}
