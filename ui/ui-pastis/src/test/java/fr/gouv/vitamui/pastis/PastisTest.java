package fr.gouv.vitamui.pastis;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.gouv.vitamui.pastis.model.ElementProperties;
import fr.gouv.vitamui.pastis.model.jaxb.*;
import fr.gouv.vitamui.pastis.util.PastisCustomCharacterEscapeHandler;
import fr.gouv.vitamui.pastis.util.PastisGetXmlJsonTree;
import fr.gouv.vitamui.pastis.util.PastisSAX2Handler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.net.URISyntaxException;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "/application.properties")
public class PastisTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(PastisTest.class);

	@Value("${rng.base.file}")
	private String rngFileName;

	@Value("${json.base.file}")
	private String jsonFileName;

	@Test
	public void testIfRngIsPresent() throws FileNotFoundException  {
		InputStream os = getClass().getClassLoader().getResourceAsStream(this.rngFileName);
	}

	@Test
	public void testIfRngCanBeGenerated() throws IOException, JAXBException {
		InputStream jsonInputStream = getClass().getClassLoader().getResourceAsStream(this.jsonFileName);
		ObjectMapper objectMapper = new ObjectMapper();
		ElementProperties jsonMap = objectMapper.readValue(jsonInputStream, ElementProperties.class);
		jsonMap.initTree(jsonMap);

		BaliseXML.buildBaliseXMLTree(jsonMap,0, null);
		BaliseXML eparentRng  = BaliseXML.baliseXMLStatic;
		JAXBContext contextObj = JAXBContext.newInstance(AttributeXML.class, ElementXML.class, DataXML.class,
				ValueXML.class, OptionalXML.class, OneOrMoreXML.class,
				ZeroOrMoreXML.class, AnnotationXML.class, DocumentationXML.class,
				StartXML.class, GrammarXML.class);
		Marshaller marshallerObj = contextObj.createMarshaller();
		marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshallerObj.setProperty("com.sun.xml.bind.marshaller.CharacterEscapeHandler",
				new PastisCustomCharacterEscapeHandler());
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Writer writer = new OutputStreamWriter(os, "UTF-8");
		marshallerObj.marshal(eparentRng, writer);
		String response = new String (os.toByteArray(), "UTF-8");
		writer.close();
		//Assert.assertFalse("RNG profile generated successfully", response.isEmpty());
	}

	@Test
	public void testIfJSONCanBeGenerated() throws IOException, JAXBException, URISyntaxException,SAXException {

		PastisSAX2Handler handler = new PastisSAX2Handler();
		PastisGetXmlJsonTree getJson = new PastisGetXmlJsonTree();

		XMLReader xmlReader = XMLReaderFactory.createXMLReader();
		xmlReader.setContentHandler(handler);

		ClassLoader loader = ClassLoader.getSystemClassLoader();

		xmlReader.parse(loader.getResource(this.rngFileName).toURI().toString());
		String jsonTree = getJson.getJsonParsedTreeTest(handler.elementRNGRoot);

		Assert.assertNotNull("JSON profile generated successfully",jsonTree);

	}



}
