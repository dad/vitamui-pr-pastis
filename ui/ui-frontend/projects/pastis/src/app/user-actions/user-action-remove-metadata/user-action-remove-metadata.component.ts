import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { PastisDialogConfirmComponent } from '../../shared/pastis-dialog/pastis-dialog-confirm/pastis-dialog-confirm.component';
import { PopupService } from '../../core/services/popup.service';

@Component({
  selector: 'pastis-user-action-remove-metadata',
  templateUrl: './user-action-remove-metadata.component.html',
  styleUrls: ['./user-action-remove-metadata.component.scss']
})
export class UserActionRemoveMetadataComponent implements OnInit {

  dataToSend:string;

  constructor(public dialogRef: MatDialogRef<PastisDialogConfirmComponent>,
    private popUpService: PopupService) { }

  ngOnInit() {
    setTimeout(() => {
      this.popUpService.setPopUpDataOnClose(this.dialogRef.componentInstance.dialogReceivedData.fileNode.name);
    },100);
  }
}
