import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { TooltipModule } from 'ng2-tooltip-directive';


import { MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatListModule,
  MatIconModule,
  MatTooltipModule,} from "@angular/material";

import { ToastrModule } from 'ngx-toastr';


import { PastisSpinnerComponent } from './pastis-spinner/pastis-spinner.component';
import { PastisUnderConstructionComponent } from './pastis-under-construction/pastis-under-construction.component';
import { PastisDialogConfirmComponent } from './pastis-dialog/pastis-dialog-confirm/pastis-dialog-confirm.component';


@NgModule({
  declarations: [
    PastisSpinnerComponent,
    PastisUnderConstructionComponent,
  ],
  imports: [CommonModule,
    FormsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatListModule,
    MatTooltipModule,
    MatTableModule,
    MatIconModule,
    MatSnackBarModule,
    TooltipModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-full-width',
      preventDuplicates: true,
      closeButton:true
    }),
],
  entryComponents: [PastisDialogConfirmComponent,PastisUnderConstructionComponent],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  exports: [
    PastisSpinnerComponent,PastisUnderConstructionComponent,TooltipModule
  ],
})
export class SharedModule {}
