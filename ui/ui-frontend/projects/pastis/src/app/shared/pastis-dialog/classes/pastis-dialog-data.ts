import { SedaData } from "../../../file-tree/classes/seda-data";
import { FileNode } from "../../../file-tree/classes/file-node";
import { ComponentType } from '@angular/cdk/portal';

export interface PastisDialogData {
    width: string;
    height: string;
    titleDialog: string;
    subTitleDialog: string;
    okLabel:string;
    cancelLabel:string;
    fileNode: FileNode;
    sedaNode: SedaData;
    disableBtnOuiOnInit:boolean;
    component: ComponentType<any>;
  }
