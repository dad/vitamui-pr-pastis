import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountComponent, ActiveTenantGuard, AppGuard, AuthGuard } from 'ui-frontend-common';
import { SedaVisualizerComponent } from './seda-visualizer/seda-visualizer.component';
import { HomeComponent } from './home/home.component';
import { ProfileVisualizerComponent } from './profile-visualizer/profile-visualizer.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard, AppGuard],
    data: { appId: 'CUSTOMERS_APP' }
      },
  {
    path: 'account',
    component: AccountComponent,
    canActivate: [AuthGuard, AppGuard],
    data: { appId: 'ACCOUNTS_APP' }
  },
  {
    path: 'sedaview',
    component: SedaVisualizerComponent,
    canActivate: [AuthGuard, AppGuard],
     data: { appId: 'CUSTOMERS_APP' }
  },
  {
    path: 'profileview',
    component: ProfileVisualizerComponent,
    canActivate: [AuthGuard, AppGuard],
     data: { appId: 'CUSTOMERS_APP' }
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    ActiveTenantGuard,
    AuthGuard
  ]
})
export class AppRoutingModule { }

export const routingComponents = [HomeComponent, SedaVisualizerComponent,ProfileVisualizerComponent];

