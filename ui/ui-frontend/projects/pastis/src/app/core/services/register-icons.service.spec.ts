import { TestBed } from '@angular/core/testing';

import { RegisterIconsService } from './register-icons.service';

describe('RegisterIconsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegisterIconsService = TestBed.get(RegisterIconsService);
    expect(service).toBeTruthy();
  });
});
