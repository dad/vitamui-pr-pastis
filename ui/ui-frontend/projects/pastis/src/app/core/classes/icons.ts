// Button names should reflect an action
export enum IconsEnum {
    'pastis-close-sidenav' = 'close-sidenav',
    'pastis-close-popup' = 'close-popup',
    'pastis-back-crete-profile' = 'arrow-back',
    'pastis-save' = 'save',
    'pastis-setting' = 'setting',
    'pastis-complex-element' = 'complex-element',
    'pastis-complex-element-white' = 'complex-element-white',
    'pastis-complex-element-white2' = 'complex-element-white2',
}
