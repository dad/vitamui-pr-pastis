import {environment as envProd} from '../../../environments/environment.prod'
import {environment as  envDev} from '../../../environments/environment.dev'
import { isDevMode, Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
export class PastisApiRoute {

    rootApiPath: string;
    apiUrls: {};
    apiPathMap : Map<string,string>;


    constructor() {
        this.rootApiPath = isDevMode() ? envDev.apiPastisUrl : envProd.apiPastisUrl;
        this.apiPathMap = new Map();
        const apiEntryPoints = new Map<string,string>()
        
        apiEntryPoints.set('createprofile','/createprofile');
        apiEntryPoints.set('updateprofile','/updateprofile');
        apiEntryPoints.set('getfile','/getFile',);
        apiEntryPoints.set('profilefromfilePath','/createprofilefromfile',);
        apiEntryPoints.set('getSedaFile','/assets/seda.json');
        
        for(var entryProint in apiEntryPoints) {
            if (apiEntryPoints.hasOwnProperty(entryProint)) {
                console.log(entryProint);
                this.apiPathMap.set(entryProint+'Path',apiEntryPoints.get(entryProint))
            }
        }


    }



}
